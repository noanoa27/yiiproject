<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180923_191322_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
		$this->createTable('user', [
				'id' => $this->primaryKey(),
				'first_name' => $this->string()->notNull(), 
				'last_name' => $this->string()->notNull(),
				'username' => $this->string()->notNull(),
				'password' => $this->string()->notNull(),
				'role' =>  "ENUM('user', 'author', 'admin')",
				'authKey' => $this->string()->notNull(),
				'accessToken' => $this->string()->notNull(),
				'email' => $this->string()->notNull(),
				//'status' => this->integer()->notNull(),
        ]);
		
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
