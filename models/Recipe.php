<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "recipe".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $ingredients
 * @property string $image
 * @property int $category
 * @property string $cooking_time
 * @property string $tags
 * @property int $posted_by
 */
class Recipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'recipe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'ingredients', 'image', 'category', 'cooking_time', 'posted_by'], 'required'],
            [['description', 'ingredients', 'image', 'tags'], 'string'],
            [['category', 'posted_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['cooking_time'], 'string', 'max' => 10],
            [['file'], 'file', 'extensions'=>'jpg, gif, png, jpeg'],
            [['file'], 'file', 'maxSize'=>'100000'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'ingredients' => 'Ingredients',
            'image' => 'Image',
            'file'=>'Picture',
            'category' => 'Category',
            'cooking_time' => 'Cooking Time',
            'tags' => 'Tags',
            'posted_by' => 'Posted By',
            'created_date'=> 'Created Date'
        ];
    }
    public function getCategory1()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    } 
}
