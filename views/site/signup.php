<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
<div class="w3_login">
            <h3><?= Html::encode($this->title) ?></h3>
            <div class="w3_login_module">
                <div class="module form-module">
                  <div class="toggle"><i class="fa fa-times"></i>
                    <div class="tooltip">Click Me</div>
                  </div>
                <div class="form">
                    <!-- <h2>Register</h2> -->
                    <?php $form = ActiveForm::begin([
                        'action' => ['users/signup'],
                        'id' => 'register-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-12 control-label'],
                        ],
                        
                    ]); ?>

                        <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
                        <?= $form->field($model, 'last_name')->textInput() ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <?= $form->field($model, 'username')->textInput() ?>
                        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                        <div class="form-group">
                            <div class="col-lg-offset-1 col-lg-11">
                                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>
                  </div>
                  
                </div>
            </div>
            
        </div>
</div>
