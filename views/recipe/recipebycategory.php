<?php
use app\models\Usermodel;
use app\models\Favorite;
?>
<div class="top-brands">
    <div class="">
        <h3>Recipe By Category</h3>
        <?php if($recipes):?>
        <div class="agile_top_brands_grids row">
            <?php foreach($recipes as $recipe):?>
                <?php $user = Usermodel::find()
                ->where(['>=', 'id', $recipe['posted_by']])
            ->all();//print_r($user);exit;
                 ?>
            <div class="col-md-3 top_brand_left">
                <div class="hover14 column">
                    <div class="agile_top_brand_left_grid">
                        <div class="agile_top_brand_left_grid1">
                            <figure>
                                <div class="snipcart-item block">
                                    <div class="snipcart-thumb">
                                        <a href="<?php echo Yii::$app->homeUrl?>recipe/view?id=<?php echo base64_encode($recipe['id'])?>"><!--<img title=" " alt=" " src="<?php echo Yii::$app->homeUrl?>uploads/<?php echo $recipe['image'];?>" height="110">--></a>        
                                        <p><?php echo $recipe['name'];?></p>
                                        <h4>
                                            <div class="postby">
                                                <span><span class="fa fa-user"></span> <?php echo $user[0]['first_name'].' '.$user[0]['last_name']?></span>
                                            </div>
                                            <!--
                                            <div class="reviewDiv">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                            -->
                                        </h4>
                                    </div>
                                    <div class="snipcart-details top_brand_home_details">
                                        
                                    <a href="<?php echo Yii::$app->homeUrl?>recipe/view?id=<?php echo base64_encode($recipe['id'])?>" class="view-more">View</a>
									 <br><br>
                                     <?php if(!Yii::$app->user->isGuest){ 
                                          $re =  Favorite::checkFavExist($recipe['id']);
                                      if($re ==0 ){


                                     ?>
									<button id="<?php echo $recipe['id']; ?>" onclick="makeItFavourite('<?php echo $recipe['id']; ?>');" class="view-more" style="background:blue;font-size: 12px">Add to Favorite</button>

                                <?php }else{ ?>
<button  class="view-more" style="background:blue;font-size: 12px">Added to list</button>

                              <?php  } ?>
                                <?php } ?>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <div class="clearfix"> </div>
        </div>
        <?php else:?>
        <p>No recipe found...</p>
    <?php endif;?>
    </div>
</div>