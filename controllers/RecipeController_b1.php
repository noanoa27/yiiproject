<?php

namespace app\controllers;

use Yii;
use app\models\Recipe;
use app\models\Review;
use app\models\Category;
use app\models\RecipeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * RecipeController implements the CRUD actions for Recipe model.
 */
class RecipeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recipe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecipeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recipe model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $id = base64_decode($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recipe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recipe();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'file');
            if (!is_null($image)) {
                $model->image = $image->name;
                $ext = pathinfo($image->name, PATHINFO_EXTENSION);
                //echo $ext = end((explode(".", $image->name)));exit;
                // generate a unique file name to prevent duplicate filenames
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/';
                $path = Yii::$app->params['uploadPath'] . $model->image;
                $image->saveAs($path);
            }
            $model->created_date = date('Y-m-d');
            $user_id = \Yii::$app->user->id;
            $model->posted_by = $user_id;
            $model->save();
            return $this->redirect(['view', 'id' => base64_encode($model->id)]);
        }
        $category = Category::find()
            ->all();

        return $this->render('create', [
            'model' => $model,
            'category' => $category
        ]);
    }
    public function actionWritereview()
    {
        //print_r($_POST);exit;
        $review = new Review();
        $review->recipe_id = $_POST['recipe_id'];
        $review->feedback = $_POST['review'];
        $review->rating = $_POST['rating'];
        echo $review->save();exit;
    }
    public function actionRecipebycategory()
    {
       $recipe= Yii::$app->db->createCommand("SELECT * FROM recipe WHERE category='".$_GET['id']."'")->queryAll();

       return $this->render('recipebycategory', [
            'recipes' => $recipe
        ]);
    }
    /**
     * Updates an existing Recipe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Recipe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Recipe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recipe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recipe::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionSearchrecipe()
    {
        $term = $_POST['searchterm'];
        $recipe= Yii::$app->db->createCommand("SELECT * from recipe WHERE recipe.name like '%".$term."%'")->queryAll();
        //echo "<pre>";
        //print_r($recipe);exit;
        return $this->render('search', [
            'recipes' => $recipe,
            'term' => $term
        ]);
    }
}
